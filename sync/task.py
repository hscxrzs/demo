# coding: utf-8
from logging import error
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.inspection import inspect
from sqlalchemy import Column, String, text
from sqlalchemy.dialects.mysql import BIGINT, LONGTEXT
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.dialects.mysql import INTEGER


from elasticsearch import Elasticsearch
from elasticsearch import helpers



Base = declarative_base()
engine = create_engine('mysql+pymysql://autolink:Platform1345@10.53.6.251:32338/al_ms_autumn', echo=True)
Session = sessionmaker(bind=engine)

with Session() as session:
        result = session.execute(
            "select user.id, user.username, `group`.id, `group`.name " +
            "from user join user_group on user.id = user_group.user_id " + 
            "join `group` on user_group.group_id = `group`.id " + 
            " where user_group.role_id = 3")
        group_to_user = dict()
        for line in result:
            group_to_user[line[2]] = line[0]
        task_executors = dict()
        result = session.execute("select resource_id, `key`, value from field_resource")
        for line in result:
            task_executors[line[0]] = line[2]


class GroupResource(Base):
    __tablename__ = 'group_resource'
    id = Column(INTEGER(10), primary_key=True)
    group_id = Column(INTEGER(11), nullable=False)
    resource_id = Column(INTEGER(11), nullable=False)
    resource_type = Column(String(255), nullable=False)
    role_id = Column(INTEGER(11), nullable=False)


class Sync:
    def auth(self, session, resource_id):
        owner_id = 0
        resource_type = self.__resource_type_mapping__[self.type]
        resp = []
        for authItem in session.query(GroupResource).filter_by(
            resource_type=resource_type, resource_id=resource_id):
            resp.append({
                "group_id": authItem.group_id,
                "role_id": authItem.role_id,
            })
            if authItem.role_id == 7:
                owner_id = group_to_user[authItem.group_id]
        return resp, owner_id

    def __str__(self):
        columns = [column.name for column in inspect(self).c]
        print("{} columns are: {}".foramt(self.__tablename__, columns))

    
    def json(self, session):
        data = dict()
        data['group_roles'], data['owner_id'] = self.auth(session, self.id)
        for column in self.__table__.columns:
            if self.__columns__ and column.name not in self.__columns__:
                continue
            column_value = getattr(self, column.name)
            if column_value is None:
                raise Exception("data is None", self.id, self.__tablename__)
            if self.__columns_mapping__ is None:
                data[column.name] = column_value
            else:
                column_key = self.__columns_mapping__.get(column.name, column.name)
                data[column_key] = column_value
        executor = task_executors.get(self.id)
        if executor:
            data['extra_fields'] = {"executor_id": task_executors[self.id]}
        return data

class Task(Base, Sync):
    __tablename__ = 'task'

    __columns__ = ['id', 'type', 'description', 'pipeline_id', 'submit_time', 'status', 'duration', 'delete_time']

    __columns_mapping__ = {
        'submit_time': 'create_time',
        'description': 'desc', 
    }

    __resource_type_mapping__ = {
        "task": "task",
        "subtask": "task-subtask",
        "workspace": "task-workspace",
    }
    id = Column(BIGINT(20), primary_key=True)
    pipeline_id = Column(BIGINT(20), nullable=False)
    pipeline_type = Column(String(255), nullable=False, server_default=text("''"), comment='pipeline类型,可选值有<common,installed>,缺省代表common')
    type = Column(String(255), nullable=False, server_default=text("''"), comment='task类型,可选值有<task,test,subtask>,缺省代表task')
    refer_task = Column(BIGINT(20), nullable=False, server_default=text("'0'"), comment='reference task id')
    status = Column(String(1024), nullable=False, server_default=text("''"))
    description = Column(String(1024), nullable=False, server_default=text("''"))
    scheduler = Column(LONGTEXT)
    schedulers = Column(LONGTEXT)
    entrypoints = Column(LONGTEXT)
    environment = Column(LONGTEXT)
    flow_data = Column(LONGTEXT)
    context_data = Column(LONGTEXT)
    variable_data = Column(LONGTEXT)
    submit_time = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    start_time = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    end_time = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    duration = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    duration_refresh = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    duration_done = Column(BIGINT(20), nullable=False, server_default=text("'0'"))
    kill_time = Column(BIGINT(20), server_default=text("'0'"))
    delete_time = Column(BIGINT(20), server_default=text("'0'"))

    def columns(self):
        return self.__table__.columns


if __name__ == '__main__':
    def generator():
        with Session() as session:
            for task in session.query(Task).order_by(Task.id):
                data = task.json(session)
                _id = data.pop('id')
                yield {
                    "_index": "resource-autumn-task",
                    "_type": "_doc",
                    "_id": _id,
                    "_source": data
                }
    
    es = Elasticsearch([{
            "host": "es.parrots.sensetime.com",
            "port": 80,
            "http_auth": "admin:6uejyw52"
        }])
    success, errors = helpers.bulk(es, generator())
    print(success, error)


"""
{
          "create_time" : 1639727419942,
          "delete_time" : 0,
          "desc" : "",
          "duration" : 0,
          "pipeline_id" : 120,
          "status" : "Succeeded",
          "type" : "workspace",
          "extra_fields" : {
            "executor_id" : "workspace-44"
          },
          "group_roles" : [
            {
              "group_id" : 1,
              "role_id" : 7
            }
          ]
        }
"""