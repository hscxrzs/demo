import logging
import time
import uuid
import json
import pymysql
from elasticsearch import Elasticsearch
from elasticsearch import helpers


def get_resource_auth(conn, resource_type, resource_id, owner_id=7,
                      db='al_ms_winter'):
    """

    :param conn:
    :param resource_type:
    :param resource_id:
    :param owner_id:
    :param db: 默认是winter环境的表格
    :return:
    """
    # 找到资源的owner所属的group
    conn.execute(
        "select group_id from {db}.group_resource where resource_type = '{}' and resource_id = {} and role_id = {}".format(
            resource_type, resource_id, owner_id, db=db)
    )
    tmp = conn.fetchone()
    if not tmp:
        return [], 0
    owner_group_id = tmp[0]

    # 找到资源的owner
    conn.execute(
        "select id from {db}.user where username=(select name from {db}.`group` where id={})".format(
            owner_group_id, db=db))
    owner_id = conn.fetchone()[0]

    # 找到资源有权限的group及其角色
    conn.execute(
        "select group_id, role_id from {db}.group_resource where resource_type = '{}' and resource_id = {}".format(
            resource_type, resource_id, db=db)
    )

    group_roles = [{'group_id': record[0], 'role_id': record[1]} for record in
                   conn.fetchall()]
    return group_roles, owner_id


# group
def generate_group():
    pass


# component
def generate_component(conn, src_db, auth_db):
    conn.execute(
        "select id, name, module, description, create_time, update_time, delete_time from {db}.component".format(
            db=src_db))

    for _id, name, module, description, create_time, update_time, delete_time in conn.fetchall():
        try:
            group_roles, owner_id = get_resource_auth(conn, 'component', _id, db=auth_db)
            body = {
                "create_time": create_time,
                "update_time": update_time,
                "delete_time": delete_time,
                "group_ids": [item['group_id'] for item in group_roles],
                "group_roles": group_roles,
                "module": module,
                "name": name,
                "desc": description
            }
            if group_roles:
                body["owner_id"] = owner_id
            yield str(_id), body
        except Exception:
            print('id is {}'.format(_id))


# pipeline
def generate_pipeline(conn, src_db, auth_db, tag_db):
    conn.execute(
        "select id, name, description, create_time, update_time, delete_time from {db}.pipeline".format(db=src_db))

    for id, name, description, create_time, update_time, delete_time in conn.fetchall():
        print('id is {}'.format(id))
        group_roles, owner_id = get_resource_auth(conn, 'pipeline', id, db=auth_db)
        conn.execute(
            "select distinct tag "
            "from {db}.tag_resource "
            "where resource_type = 'pipeline' "
            "and resource_id={}".format(id, db=tag_db))

        body = {
            "name": name,
            "desc": description,
            "create_time": create_time,
            "update_time": update_time,
            "delete_time": delete_time,
            "group_ids": [item['group_id'] for item in group_roles],
            "group_roles": group_roles,
            "tags": [item[0] for item in conn.fetchall()],
        }
        if group_roles:
            body["owner_id"] = owner_id
        yield str(id), body


# task
def generate_task(conn, src_db, auth_db):
    conn.execute(
        "select id, type, description, pipeline_id, submit_time, status, duration, delete_time from {db}.task".format(
            db=src_db))
    for _id, type, description, pipeline_id, submit_time, status, duration, delete_time in conn.fetchall():
        # 更新类型是task的数据
        print('task_id is {}'.format(_id))
        if type in ['task', '', 'partial']:
            group_roles, owner_id = get_resource_auth(conn, 'task', _id,
                                                      db=auth_db)
            body = {
                "desc": description,
                "pipeline_id": pipeline_id,
                "create_time": submit_time,
                "delete_time": delete_time,
                "status": status,
                "group_roles": group_roles,
                "group_ids": [item['group_id'] for item in group_roles],
                "duration": duration,
            }
            if group_roles:
                body['owner_id'] = owner_id

            yield str(_id), body


def generate_sub_task(conn, src_db, auth_db):
    conn.execute(
        "select id, type, description, pipeline_id, submit_time, status, duration, delete_time from {db}.task where type='subtask'".format(
            db=src_db))
    for _id, type, description, pipeline_id, submit_time, status, duration, delete_time in conn.fetchall():
        # 更新类型是task的数据

        if type == 'subtask':
            print('task_id is {}'.format(_id))
            group_roles, owner_id = get_resource_auth(conn, 'task-subtask', _id,
                                                      db=auth_db)
            body = {
                "desc": description,
                "pipeline_id": pipeline_id,
                "create_time": submit_time,
                "delete_time": delete_time,
                "status": status,
                "group_roles": [],
                "group_ids": [],
                "duration": duration,
            }
            if group_roles:
                body['owner_id'] = owner_id

            yield str(_id), body


# task-test
def generate_task_test(conn, src_db, auth_db):
    conn.execute(
        "select id, type, description, pipeline_id, submit_time, status, duration, delete_time from {}.task where type='test'".format(
            src_db))
    for _id, type, description, pipeline_id, submit_time, status, duration, delete_time in conn.fetchall():
        print("task-test id is : ", _id)
        if type == 'test':
            group_roles, owner_id = get_resource_auth(conn, 'task-test', _id,
                                                      db=auth_db)
            body = {
                "desc": description,
                "pipeline_id": pipeline_id,
                "create_time": submit_time,
                "delete_time": delete_time,
                "status": status,
                "group_roles": group_roles,
                "group_ids": [item['group_id'] for item in group_roles],
                "duration": duration,
            }
            if group_roles:
                body["owner_id"] = owner_id
            yield str(_id), body


# tag
def generate_tag(conn, src_db):
    conn.execute(
        "select id, tag, resource_type from {db}.tag order by id".format(db=src_db))
    for _id, tag, resource_type in conn.fetchall():
        print("tag id is : ", _id)
        body = {
            "tag": tag,
            "resource_type": resource_type
        }
        yield str(_id), body


# project
def generate_project(conn, src_db, auth_db, tag_db):
    conn.execute(
        "select id, name, description, created, updated, deleted from {}.pavi_project".format(src_db))

    for _id, name, description, created, updated, deleted in conn.fetchall():
        print('id is {}'.format(_id))
        group_roles, owner_id = get_resource_auth(conn, 'project', _id, db=auth_db)
        conn.execute(
            "select distinct tag "
            "from "
            "{tag_db}.tag_resource "
            "where resource_type = 'project' "
            "and resource_id={}".format(_id, tag_db=tag_db))
        # conn.execute(
        #     "select distinct tag "
        #     "from "
        #     "{tag_db}.tag_resource left join {tag_db}.tag on tag_resource.tag_id = tag.id "
        #     "where tag_resource.resource_type = 'project' "
        #     "and tag_resource.resource_id={}".format(_id, tag_db=tag_db))

        body = {
            "create_time": created,
            "update_time": updated,
            "delete_time": deleted,
            "group_ids": [item['group_id'] for item in group_roles],
            "group_roles": group_roles,
            "tags": [record[0] for record in conn.fetchall()],
            "name": name,
            "desc": description
        }
        if group_roles:
            body["owner_id"] = owner_id
        yield str(_id), body


# training
def generate_training(conn, src_db, tag_db):
    conn.execute("select id, name from {db}.pavi_project".format(db=src_db))
    project_map = {}
    for _id, name in conn.fetchall():
        project_map[_id] = name
    conn.execute("select max(id) from {db}.pavi_training".format(db=src_db))
    count = conn.fetchone()[0]
    for times in range(int(count / 500 + 1)):
        start_index = times * 500
        end_index = (times + 1) * 500
        print("index range [{}, {})".format(start_index, end_index))
        sql = "select id, name, owner_id, project_id, description, created, updated, deleted, has_res from {db}.pavi_training where id>={s_id} and id < {e_id}".format(
            s_id=start_index, e_id=end_index, db=src_db)

        conn.execute(sql)

        for _id, name, owner_id, project_id, description, created, updated, deleted, has_res in conn.fetchall():
            if project_id not in project_map.keys():
                continue
            # if owner_id != 1 or description=='':
            #     continue
            project_name = project_map[project_id]
            # conn.execute("select name from pavi_project where id={}".format(project_id))
            # project_name = conn.fetchone()[0]
            # print(_id)
            conn.execute(
                "select distinct tag "
                "from "
                "{tag_db}.tag_resource "
                "where resource_type = 'training' "
                "and resource_id={}".format(_id, tag_db=tag_db))
            # conn.execute(
            #     "select distinct tag "
            #     "from "
            #     "{tag_db}.tag_resource left join {tag_db}.tag on tag_resource.tag_id = tag.id "
            #     "where tag_resource.resource_type = 'training' "
            #     "and tag_resource.resource_id={}".format(_id, tag_db=tag_db))

            tags = [record[0] for record in conn.fetchall()]
            body = {
                "create_time": created,
                "update_time": updated,
                "delete_time": deleted,
                "desc": description,
                "name": name,
                "owner_id": owner_id,
                "project_id": project_id,
                "project_name": project_name,
                "tags": tags,
                "has_res": has_res
            }
            yield str(_id), body


# compare
def generate_compare(conn, src_db, auth_db, tag_db):
    conn.execute(
        "select id, owner_id, owner_name, name, description, created, updated, deleted from {db}.pavi_compare".format(
            db=src_db))

    for _id, owner_id, owner_name, name, description, created, updated, deleted in conn.fetchall():
        print('id is {}'.format(_id))
        group_roles, owner_id = get_resource_auth(conn, 'compare', _id, db=auth_db)
        conn.execute(
            "select distinct tag "
            "from {db}.tag_resource "
            "where resource_type = 'compare' "
            "and resource_id={}".format(_id, db=tag_db))
        # conn.execute(
        #     "select distinct tag "
        #     "from "
        #     "{tag_db}.tag_resource left join {tag_db}.tag on tag_resource.tag_id = tag.id "
        #     "where tag_resource.resource_type = 'compare' "
        #     "and tag_resource.resource_id={}".format(_id, tag_db=tag_db))

        body = {
            "name": name,
            "desc": description,
            "create_time": created,
            "update_time": updated,
            "delete_time": deleted,
            "group_ids": [item['group_id'] for item in group_roles],
            "group_roles": group_roles,
            "tags": [item[0] for item in conn.fetchall()],
        }
        if group_roles:
            body["owner_id"] = owner_id
        yield str(_id), body


# resource
def generate_resource(conn, src_db, start_id=3000000, end_id=3300000):
    resource_type_mapping = {
        0: "Unknown",
        1: "Graph",
        2: "Feature",
        3: "Histogram",
        4: "Snapshot",
        5: "Heatmap",
        6: "Timeline",
        7: "Image3D",
        8: "DArray",
        9: "Text",
        100: "Tensor",
    }

    conn.execute(
        "select id, owner_id, name, project_id, deleted from {db}.pavi_training".format(db=src_db))
    trainings = dict()
    for training_id, training_owner_id, training_name, training_project_id, deleted in conn.fetchall():
        trainings[training_id] = {
            'owner': training_owner_id,
            'name': training_name,
            'project_id': training_project_id,
            "deleted": deleted != 0,
        }
    conn.execute("select id, name from {db}.pavi_project".format(db=src_db))
    projects = dict()
    for project_id, project_name in conn.fetchall():
        projects[project_id] = {
            'name': project_name,
        }

    # conn.execute("select max(id) from {db}.pavi_resource".format(db=src_db))
    # count = conn.fetchone()[0]

    # count = end_id - start_id
    count = 500

    for times in range(int(count / 500)):
        start_index = times * 500 + start_id
        end_index = (times + 1) * 500 + start_id

        print("index range [{}, {})".format(start_index, end_index))
        # sql = "select id, tag, training_id, type, tag, iteration, size, created, deleted from {db}.pavi_resource where id>={} and id < {}".format(
        #     start_index, end_index, db=src_db)

        sql = "select id, tag, training_id, type, tag, iteration, size, created, deleted from {db}.pavi_resource where deleted>1635480000000".format(
            start_index, end_index, db=src_db)

        conn.execute(sql)
        records = conn.fetchall()
        # print(len(records), start_index, end_index, records[-1][0])

        # yield
        if not records:
            continue
        for record in records:
            _id, tag, training_id, _type, tag, iteration, size, create_time, delete_time = record
            # if delete_time != 0:
            #     print("deleted record ", record)
            #     continue

            body = {
                "create_time": create_time,
                "delete_time": delete_time,
                "iter": iteration,
                "name": tag,
                "type": resource_type_mapping[_type],
                "size": size,
                "training_id": training_id,
                "training_name": trainings[training_id]['name'],
                "training_owner": trainings[training_id]['owner'],
                "project_id": trainings[training_id]['project_id'],
                "project_name": projects[trainings[training_id]['project_id']][
                    'name'],
            }
            yield str(_id), body


# model
# public-model
# machine
def generate_machine(conn, src_db, auth_db):
    conn.execute(
        "select id, name, os, arch, description, created, updated, deleted from {db}.machine".format(db=src_db))

    for _id, name, os, arch, description, created, updated, deleted in conn.fetchall():
        print('id is {}'.format(_id))
        group_roles, owner_id = get_resource_auth(conn, 'machine', _id, db=auth_db)
        body = {
            "group_ids": [item['group_id'] for item in group_roles],
            "group_roles": group_roles,
            "name": name,
            'os': os,
            'arch': arch,
            "desc": description,
            "create_time": created,
            "update_time": updated,
            "delete_time": deleted
        }
        if group_roles:
            body["owner_id"] = owner_id
        yield str(_id), body


# node
# leaderboard
def generate_leaderboard(conn, src_db, auth_db):
    conn.execute(
        "select id, owner_id, owner_name, name, description, created, updated, deleted from {db}.leaderboard".format(
            db=src_db))

    for _id, owner_id1, owner_name, name, description, created, updated, deleted in conn.fetchall():
        print('id is {}'.format(_id))
        group_roles, owner_id = get_resource_auth(conn, 'leaderboard', _id, db=auth_db)
        body = {
            "name": name,
            "owner_name": owner_name,
            "desc": description,
            "create_time": created,
            "update_time": updated,
            "delete_time": deleted,
            "group_roles": group_roles,
            "owner_id": owner_id1
        }
        yield str(_id), body


# node-report-record


class SyncHandler:
    def __init__(self, my_host, my_user, my_pwd, my_port, es_host, es_port, es_http_auth, env="winter"):
        self.conn = pymysql.connect(host=my_host, user=my_user,
                                    passwd=my_pwd, port=my_port).cursor()
        self.es = Elasticsearch([{
            "host": es_host,
            "port": es_port,
            "http_auth": es_http_auth
        }])
        data_index = ["group", "component", "pipeline", "task", "task_test", "tag", "project", "training",
                      "compare", "resource", "machine", "leaderboard", "auth_db", "tag_db", "sub_task"]
        if env in ["spring", "summer", "autumn", "winter"]:
            self.db_map = {}
            for item in data_index:
                self.db_map[item] = "al_ms_{}".format(env)
        elif env == "prod":
            self.db_map = {
                "auth_db": "al_ms_auth", "tag_db": "al_ms_tag", "group": "al_ms_auth", "component": "al_ms_component",
                "pipeline": "al_ms_workflow", "task": "al_ms_workflow", "task_test": "al_ms_workflow",
                "sub_task": "al_ms_workflow",
                "tag": "al_ms_tag", "project": "al_ms_pavi", "training": "al_ms_pavi", "compare": "al_ms_pavi",
                "resource": "al_ms_pavi", "machine": "al_ms_machine", "leaderboard": "al_ms_artifacts"
            }
        else:
            raise IndexError("环境找不到")

    def sync_data(self, data_name, dst_index):
        data = self.gen_data(data_name)

        def generator():
            for _id, ele in data:
                # yield {
                #     "update": {
                #         "_index": dst_index,
                #         "_id": id,
                #     }
                # }
                yield {
                        "_index": dst_index,
                        "_type": "_doc",
                        "_id": _id,
                        "_source": ele
                    }
                # yield {
                #     '_op_type': 'update',
                #     '_index': dst_index,
                #     '_type': "doc",
                #     '_id': _id,
                #     'doc': ele
                # }

        start_time = time.time()
        success, errors = helpers.bulk(self.es, generator())
        # self.es.bulk()
        print(success, errors)
        print("cost time is ", time.time() - start_time)

    def _sync(self, data, dst_index):
        data = list(map(lambda x: {
                    '_op_type': 'update',
                    '_index': dst_index,
                    '_type': "_doc",
                    '_id': int(x[0]),
                    'doc': x[1]
                }, data))
        # print(data[0])
        # data = [
        #     {
        #         '_op_type': 'update',
        #         '_index': dst_index,
        #         '_type': 'doc',
        #         '_id': 42,
        #         'doc': {'question': '1'}
        #     },
        #     {
        #         '_op_type': 'update',
        #         '_index': dst_index,
        #         '_type': 'doc',
        #         '_id': 21,
        #         'doc': {
        #             'desc': 'jeorch test task',
        #             'pipeline_id': 1,
        #             'create_time': 1612510439262,
        #             'delete_time': 0,
        #              'status': 'Running',
        #             'duration': 24436619210,
        #             'owner_id': 3
        #         }
        #     }
        # ]
        start_time = time.time()
        success, errors = helpers.bulk(self.es, data, raise_on_exception=False, raise_on_error=False)
        # for item in data:
        #     print(item["doc"])
        #     self.es.update(index=dst_index, id=item['_id'], body=item['doc'])
        # # self.es.bulk()
        print(success, errors)
        print("cost time is ", time.time() - start_time)
    def save_to_file(self, data_name, filename="tmp.txt"):
        data = self.gen_data(data_name)
        with open(filename, "w", encoding="utf-8") as f:
            for item in data:
                f.write(json.dumps(item) + '\n')

    def sync_from_file(self, data_name, filename="tmp.txt", chunk_size=10):
        # self._sync([], "test")
        with open(filename, 'r', encoding="utf-8") as f:
            cnt = 0
            data_list = []
            for line in f:
                data_list.append(json.loads(line.strip()))
                cnt += 1
                if cnt == chunk_size:
                    self._sync(data_list, "autolink-winter-task")
                    cnt = 0
                    data_list = []
            if data_list:
                self._sync(data_list, "autolink-winter-task")

    def print_sync_data(self, data_name, limit=1):
        data = self.gen_data(data_name)
        cnt = 0
        for i in data:
            print(i)
            cnt += 1
            if cnt >= limit:
                break

    def gen_data(self, data_name):
        sync_fun = eval("generate_{}".format(data_name))
        func_args = sync_fun.__code__.co_varnames[:sync_fun.__code__.co_argcount]
        func_kwargs = {
            "conn": self.conn,
            "src_db": self.db_map[data_name]
        }
        if 'tag_db' in func_args:
            func_kwargs['tag_db'] = self.db_map['tag_db']
        if 'auth_db' in func_args:
            func_kwargs['auth_db'] = self.db_map['auth_db']
        data = sync_fun(**func_kwargs)
        return data


if __name__ == '__main__':
    test_env = {}
    # env = "summer"
    env = "prod"

    # handler = SyncHandler("bj.pass.sensetime.com", "autolink", "Autolink2021", 37038, "es.parrots.sensetime.com", 80,
    #                       "admin:6uejyw52", env=env)
    handler = SyncHandler("bj.pass.sensetime.com", "autolink", "Platform1345", 32338, "es.parrots.sensetime.com", 80,
                          "admin:6uejyw52", env=env)
    # handler = SyncHandler("bj.pass.sensetime.com", "autolink", "Platform1345", 32338, "localhost", 9200,
    #                       "elastic:123456", env=env)
    # "component", "pipeline", "task", "task_test", "tag",
    # handler.print_sync_data("pipeline", limit=200)
    resource = "task_test"
    # 10137+39
    # handler.print_sync_data(resource, limit=150)
    handler.sync_data(resource, "resource-prod-task-test")
    # handler.sync_data(resource, "resource-{}-{}".format(env, "task-test"))
    # handler.save_to_file("task")
    # handler.sync_from_file("pipeline")
    # ret = handler.es.get("autolink-winter-task", 1719)
    # print(ret)
    # handler.print_sync_data("sub_task", limit=200)
    # for data_name in ["project", "training",
    #               "compare", "machine", "leaderboard"]:
    #     print("start {} migrate".format(data_name).center(100, "="))
    #     dst_index = "resource-{}-{}".format(env, data_name if data_name != "task_test" else "task-test")
    #     print(dst_index)
    #     # handler.print_sync_data(data_name, limit=1)
    #     handler.sync_data(data_name, dst_index=dst_index)
    #     print("{} migrate end".format(data_name).center(100, "="))
