package main

import (
	"fmt"
	"sync"

	"github.com/casbin/casbin"
)

type CasbinEnforcer struct {
	casbinEnforcer *casbin.Enforcer
}

//Auth resource
func (en *CasbinEnforcer) Auth(sub, obj, perm string) bool {
	res := en.casbinEnforcer.Enforce(sub, obj, perm)
	return res
}

//get sub resources
func (en *CasbinEnforcer) GetResources(sub string) [][]string {
	res := en.casbinEnforcer.GetImplicitPermissionsForUser(sub)
	return res
}

var (
	onceCasbin     sync.Once
	casbinEnforcer *CasbinEnforcer
)

//NewCasbin get casbin enforcer
func NewCasbin() *CasbinEnforcer {
	onceCasbin.Do(func() {
		a := NewAdapter("mysql", "root:hscxrzs1st@tcp(127.0.0.1:3306)/casbin")
		casbinEnforcer = &CasbinEnforcer{
			casbinEnforcer: casbin.NewEnforcer("casbin/rbac_model.conf", a),
		}
		casbinEnforcer.casbinEnforcer.LoadPolicy()
	})
	return casbinEnforcer
}

func main() {
	fmt.Println(NewCasbin().Auth("OWNER", "PIPELINE", "RUN"))
	for _, line := range NewCasbin().GetResources("TEST") {
		fmt.Println(line)
	}
}
