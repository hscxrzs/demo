package main

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"

	"github.com/casbin/casbin/model"
	_ "github.com/go-sql-driver/mysql"
)

// Adapter represents the MySQL adapter for policy storage.
type Adapter struct {
	driverName     string
	dataSourceName string
	db             *sql.DB
}

// NewAdapter is the constructor for Adapter.
func NewAdapter(driverName string, dataSourceName string) *Adapter {
	a := Adapter{}
	a.driverName = driverName
	a.dataSourceName = dataSourceName
	return &a
}

func (a *Adapter) open() {
	db, err := sql.Open(a.driverName, a.dataSourceName)
	if err != nil {
		panic(err)
	}
	if err := db.Ping(); err != nil {
		fmt.Println("open database fail")
		return
	}
	a.db = db
}

func (a *Adapter) close() {
	a.db.Close()
}

func loadPolicyLine(line string, model model.Model) {
	if line == "" {
		return
	}

	tokens := strings.Split(line, ", ")

	key := tokens[0]
	sec := key[:1]
	model[sec][key].Policy = append(model[sec][key].Policy, tokens[1:])
}

// LoadPolicy loads policy from database.
func (a *Adapter) LoadPolicy(model model.Model) error {
	a.open()
	defer a.close()

	var (
		subject string
		object  string
		action  string
	)

	rows, err := a.db.Query("select subject, object, action from policy")
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&subject, &object, &action)
		if err != nil {
			return err
		}
		line := fmt.Sprintf("p, %v, %v, %v", subject, object, action)
		loadPolicyLine(line, model)
	}
	err = rows.Err()
	return err
}

// SavePolicy saves policy to database.
func (a *Adapter) SavePolicy(model model.Model) error {
	return errors.New("not implemented")
}

func (a *Adapter) AddPolicy(sec string, ptype string, policy []string) error {
	return errors.New("not implemented")
}

func (a *Adapter) RemovePolicy(sec string, ptype string, policy []string) error {
	return errors.New("not implemented")
}

func (a *Adapter) RemoveFilteredPolicy(sec string, ptype string, fieldIndex int, fieldValues ...string) error {
	return errors.New("not implemented")
}
