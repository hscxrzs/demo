package util

import (
	"fmt"
	"time"
)

func RunCmd(args ...string) {
	c := make(chan error)
	go func() {
		a := <- c
		fmt.Println("a is ", a)
	}()
	select {
	case c <- nil:
		fmt.Println("channel sended nil")
	case <-time.After(time.Second):
		fmt.Println("default")
	}
	//cmd := exec.Command("ls", "-lah")
	//var stdout, stderr bytes.Buffer
	//cmd.Stdout = &stdout
	//cmd.Stderr = &stderr
	//err := cmd.Run()
	//fmt.Println("cmd.process is ", cmd.Process)
	//if err != nil {
	//	fmt.Printf("cmd.Run() failed with %s\n", err)
	//}
	//fmt.Printf("out:\n%s\nerr:\n%s\n", string(stdout.Bytes()), string(stderr.Bytes()))
}

func WriteToFile() {

}
