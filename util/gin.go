package util

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"strings"
	"time"
)

type Info struct {
	Name string `form:"name"`
	Age  int    `form:"age"`
}

func generateLimitKey(ip string) string {
	timestamp := time.Now().Unix()
	if idx := strings.Index(ip, ":"); idx > 0 {
		ip = ip[:idx]
	}
	fmt.Println()
	fmt.Println()
	return fmt.Sprintf("rate_limit:%v:%v", timestamp, ip)
}

func RunGin() {
	httpServer := gin.New()
	httpServer.Use(gin.Recovery())
	httpServer.Handle("GET", "/info", func(c *gin.Context) {
		c.JSON(200, map[string]interface{}{
			"name": "name",
			"age":  "age",
		})
	})
	httpServer.Handle("POST", "/api/limit",
		//func(c *gin.Context) {
		//	conn := client.Get()
		//	defer conn.Close()
		//	redisKey := generateLimitKey(c.ClientIP())
		//	res, err := redis.Int(conn.Do("incr", redisKey))
		//	if err != nil {
		//		return
		//	}
		//	_, _ = conn.Do("expire", redisKey, 3)
		//	if res > 10 {
		//		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"success": false, "msg": "request too quickly"})
		//	}
		//},
		func(c *gin.Context) {
			info := Info{}
			_ = c.BindJSON(&info)
			c.JSON(200, info)
		})

	err := httpServer.Run("127.0.0.1:8080")
	if err != nil {
		fmt.Println("err is ", err)
	}
}
