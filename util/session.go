package util

import (
	"fmt"

	"github.com/astaxie/beego/session"
	_ "github.com/astaxie/beego/session/redis"
	log "github.com/sirupsen/logrus"
)

var (
	GlobalSessions *session.Manager
	SessionConfig  *session.ManagerConfig
)

func InitSession() {
	SessionConfig = &session.ManagerConfig{
		CookieName:      "demo",
		EnableSetCookie: true,
		Gclifetime:      3600,
		Maxlifetime:     3600,
		Secure:          false,
		CookieLifeTime:  0, // CookieLifeTime 控制客户端cookie的存在时间，设置为0默认为浏览器关闭之前都有效
		Domain:          "127.0.0.1",
		ProviderConfig:  fmt.Sprintf("%s,%s", "127.0.0.1:6379", "10"),
	}
	GlobalSessions, _ = session.NewManager("redis", SessionConfig)
	go GlobalSessions.GC()
	log.Infoln("util session ok")
}
