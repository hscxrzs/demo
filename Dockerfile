FROM golang:alpine
WORKDIR /app
ADD . /app
RUN cd /app && go build -o goapp main.go
EXPOSE 8080
ENTRYPOINT ./goapp